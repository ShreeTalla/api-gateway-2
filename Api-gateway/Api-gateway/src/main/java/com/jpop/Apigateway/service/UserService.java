package com.jpop.Apigateway.service;

import com.jpop.Apigateway.entity.UserCredentialsDAO;
import com.jpop.Apigateway.entity.UserCredentialsDTO;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    UserCredentialsDAO save(UserCredentialsDTO user);

    UserCredentialsDAO findOne(String username);
}
